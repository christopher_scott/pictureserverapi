using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Data
{
    public class FriendsRepository : IFriendsRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public FriendsRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<FriendForClientDto> Create(FriendForCreateDto friend)
        {
            // you aren't checking if these users exist anywhere. Fix that.
            var friendToCreate = new Friend {
                UserId = friend.UserId,
                UserIdOfFriend = friend.UserIdOfFriend,
                DateFriended = DateTime.UtcNow
            };
            await _context.Friends.AddAsync(friendToCreate);
            await _context.SaveChangesAsync();

            var friendToReturn = _mapper.Map<FriendForClientDto>(friendToCreate);

            return friendToReturn;
        }

        public async Task Delete(FriendForCreateDto friend)
        {
            // there's a 500 if you try to delete a friend that doesn't exist. Fix this.
            var friendToDelete = await _context.Friends.FirstOrDefaultAsync(
                    f => f.UserId == friend.UserId && f.UserIdOfFriend == friend.UserIdOfFriend ||
                    f.UserId == friend.UserIdOfFriend && f.UserIdOfFriend == friend.UserId
                );
            _context.Remove(friendToDelete);
            await _context.SaveChangesAsync();
        }

        // public async Task<IEnumerable<FriendForClientDto>> GetFriendsByUserId(int id)
        // {
        //     var friends = await _context.Friends.Where(f => f.UserId == id || f.UserIdOfFriend == id).ToListAsync();
        //     var friendsToReturn = _mapper.Map<IEnumerable<FriendForClientDto>>(friends);
        //     return friendsToReturn;
        // }

        public async Task<IEnumerable<UserForClient>> GetFriendsByUserId(int id)
        {
            var friendIds = await GetFriendsIdsByUserId(id);
            var friends = await _context.Users.Where(u => friendIds.Contains((int)u.UserId)).ToListAsync();
            var friendsToReturn = _mapper.Map<IEnumerable<UserForClient>>(friends);
            return friendsToReturn;
        }

        public async Task<List<int>> GetFriendsIdsByUserId(int id)
        {
            var friends = await _context.Friends.Where(f => f.UserId == id || f.UserIdOfFriend == id).ToListAsync();
            var userIds = friends.Select(f => f.UserId).ToList();
            var userIdsOfFriends = friends.Select(f => f.UserIdOfFriend).ToList();

            List<int> userIdsMinusUser = userIds.Where(u => u != id).ToList();
            List<int> userIdsOfFriendsMinusUser = userIdsOfFriends.Where(u => u != id).ToList();

            var friendsToReturn = new List<int>();
            foreach (int i in userIdsMinusUser)
            {
                friendsToReturn.Add(i);
            }

            foreach (int i in userIdsOfFriendsMinusUser)
            {
                friendsToReturn.Add(i);
            }
            return friendsToReturn;
        }

        public Task Save()
        {
            throw new System.NotImplementedException();
        }

        // This should probably be in a service or domain layer
        public async Task<bool> FriendRelationshipExists(int firstId, int secondId)
        {
            // this may not be right how you have this for f.User.UserId
            if (await _context.Friends.AnyAsync(f => f.UserId == firstId && f.UserIdOfFriend == secondId))
                return true;
            
            if (await _context.Friends.AnyAsync(f => f.UserId == secondId && f.UserIdOfFriend == firstId))
                return true;
            
            return false;
        }
    }
}