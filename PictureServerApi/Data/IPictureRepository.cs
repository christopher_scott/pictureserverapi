using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Data
{
    public interface IPictureRepository
    {
        Task<List<Picture>> GetAllPictures();
        Task<List<Picture>> GetPicturesByUserId(int userId);
        Task<Picture> GetPictureById(int id);
        Task<PictureForClientDto> Create(Picture picture);
        //void Update(int id, UserUpdate userUpdate);
        Task Delete(int id);

        Task Save();
    }
}
