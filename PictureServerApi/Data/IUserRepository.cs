using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using PictureServerApi.Dtos;

namespace PictureServerApi.Data
{
    public interface IUserRepository
    {
        Task<List<UserForClient>> GetAllUsers();
        Task<UserForClient> GetUserById(int id);
        Task<UserForClient> UpdateUser(int id, JsonPatchDocument<UserForPatchDto> patchModel);
    }
}