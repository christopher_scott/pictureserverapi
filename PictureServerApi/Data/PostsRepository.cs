using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Data
{
    public class PostsRepository : IPostsRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IFriendsRepository _friendsRepo;

        public PostsRepository(DataContext context, IMapper mapper, IFriendsRepository friendsRepo)
        {
            _friendsRepo = friendsRepo;
            _context = context;
            _mapper = mapper;
        }
        public async Task<PostForClient> Create(Post post)
        {
            await _context.Posts.AddAsync(post);
            await Save();
            _context.Entry(post)
                .Reference(p => p.User)
                .Load();

            var postForClient = _mapper.Map<PostForClient>(post);
            return postForClient;
        }

        public async Task Delete(int id)
        {
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.PostId == id);
            _context.Remove(post);
            await Save();
        }

        public async Task<IEnumerable<PostForClient>> GetAllPosts()
        {
            var posts = await _context.Posts.Include(u => u.User).OrderByDescending(p => p.DateCreated).ToListAsync();
            var postsToReturn = _mapper.Map<IEnumerable<PostForClient>>(posts);
            return postsToReturn;
        }

        public async Task<PostForClient> GetPostById(int id)
        {
            var post = await _context.Posts.Include(u => u.User).FirstOrDefaultAsync(p => p.PostId == id);
            var postToReturn = _mapper.Map<PostForClient>(post);
            return postToReturn;
        }

        public async Task<IEnumerable<PostForClient>> GetPostsByUserId(int userId)
        {
            var posts = await _context.Posts.Include(u => u.User).Where(p => p.UserId == userId).OrderByDescending(p => p.DateCreated).ToListAsync();
            var postsToReturn = _mapper.Map<IEnumerable<PostForClient>>(posts);
            return postsToReturn;
        }

        public async Task<IEnumerable<PostForClient>> GetPostsForTimeline(int userId)
        {
            var friendIds = await _friendsRepo.GetFriendsIdsByUserId(userId);
            var posts = await _context.Posts.Include(u => u.User).Where(p => friendIds.Contains((int) p.UserId) || p.UserId == userId).OrderByDescending(p => p.DateCreated).ToListAsync();
            // var posts = await _context.Posts.Include(u => u.User).Where(p => p.UserId == userId).OrderByDescending(p => p.DateCreated).ToListAsync();
            var postsToReturn = _mapper.Map<IEnumerable<PostForClient>>(posts);
            return postsToReturn;
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}