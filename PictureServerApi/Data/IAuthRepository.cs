using System.Threading.Tasks;
using PictureServerApi.Models;

namespace PictureServerApi.Data
{
    public interface IAuthRepository
    {
        // probably, you will want to deal with email for all of these also
        Task<User> Register(User user, string password);
        Task<User> Login(string username, string password);

        Task<bool> UserExists(string username);
        Task<bool> EmailExists(string email);
    }
}