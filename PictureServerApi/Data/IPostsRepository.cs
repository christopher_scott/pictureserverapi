using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Data
{
    public interface IPostsRepository
    {
        Task<IEnumerable<PostForClient>> GetAllPosts();
        Task<IEnumerable<PostForClient>> GetPostsByUserId(int userId);
        Task<PostForClient> GetPostById(int id);
        Task<IEnumerable<PostForClient>> GetPostsForTimeline(int userId);
        Task<PostForClient> Create(Post post);
        //void Update(int id, UserUpdate userUpdate);
        Task Delete(int id);

        Task Save();
    }
}
