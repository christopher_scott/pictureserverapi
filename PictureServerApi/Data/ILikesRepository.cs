using System.Collections.Generic;
using System.Threading.Tasks;
using PictureServerApi.Dtos;

namespace PictureServerApi.Data
{
    public interface ILikesRepository
    {
        Task<IEnumerable<LikeForClientDto>> GetLikesByPostId(int id);
        Task<LikeForClientDto> Create(LikeForCreateDto like);
        Task Delete(int id);

        Task Save();
    }
}