using System.Collections.Generic;
using System.Threading.Tasks;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Data
{
    public interface IFriendsRepository
    {
        Task<IEnumerable<UserForClient>> GetFriendsByUserId(int id);
        Task<List<int>> GetFriendsIdsByUserId(int id);
        Task<FriendForClientDto> Create(FriendForCreateDto friend);
        Task Delete(FriendForCreateDto friend);
        Task Save();
        Task<bool> FriendRelationshipExists(int firstId, int secondId);
    }
}