using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UserRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<UserForClient>> GetAllUsers()
        {
            var user = await _context.Users.OrderByDescending(u => u.DateJoined).ToListAsync();
            var usersToReturn = _mapper.Map<List<UserForClient>>(user);

            return usersToReturn;
        }

        public async Task<UserForClient> GetUserById(int id)
        {
            var user = await _context.Users.Where(u => u.UserId == id).FirstOrDefaultAsync();
            var userToReturn = _mapper.Map<UserForClient>(user);

            return userToReturn;
        }

        public async Task<UserForClient> UpdateUser(int id, JsonPatchDocument<UserForPatchDto> patchModel)
        {
            var user = await _context.Users.Where(u => u.UserId == id).FirstOrDefaultAsync();
            var userForPatchDto = _mapper.Map<UserForPatchDto>(user);
            patchModel.ApplyTo(userForPatchDto);

            _mapper.Map(userForPatchDto, user);

            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            var userForClient = _mapper.Map<UserForClient>(user);
            return userForClient;
        }
    }
}