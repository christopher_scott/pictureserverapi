using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PictureServerApi.Dtos;
using PictureServerApi.Models;
using System.Linq;

namespace PictureServerApi.Data
{
    public class LikesRepository : ILikesRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public LikesRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<LikeForClientDto> Create(LikeForCreateDto like)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.UserId == like.UserId);
            var existingLike = await _context.Likes.Where(l => l.PostId == like.PostId && l.User.UserId == user.UserId ).FirstOrDefaultAsync();
            
            if (existingLike != null)
            {    
                var existingLikeToReturn = _mapper.Map<LikeForClientDto>(existingLike);      
                return existingLikeToReturn;
            }
            
            var likeToCreate = new Like { PostId = like.PostId, User = user };
            await _context.Likes.AddAsync(likeToCreate);
            await Save();
            var likeToReturn = _mapper.Map<LikeForClientDto>(likeToCreate);
            return likeToReturn;
        }

        public async Task Delete(int id)
        {
            var like = await _context.Likes.FirstOrDefaultAsync(l => l.LikeId == id);
            _context.Remove(like);
            await Save();
        }

        public async Task<IEnumerable<LikeForClientDto>> GetLikesByPostId(int id)
        {
            var likes = await _context.Likes.Include(u => u.User).Where(l => l.PostId == id).ToListAsync();
            var likesToReturn = _mapper.Map<IEnumerable<LikeForClientDto>>(likes);
            return likesToReturn;
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}