using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PictureServerApi.Dtos;
using PictureServerApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  PictureServerApi.Data
{
    public class PictureRepository : IPictureRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public PictureRepository(DataContext context,  IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PictureForClientDto> Create(Picture picture)
        {
            picture.DateUploaded = DateTime.UtcNow;
            // this is for messing around. Eventually we want this to be for the logged in user
            // if (picture.UserId < 1)
            // {
            //     picture.UserId = 1;
            // }

            await _context.Pictures.AddAsync(picture);
            await Save();
            var pictureForClient = _mapper.Map<PictureForClientDto>(picture);
            return pictureForClient;
        }

        public async Task<List<Picture>> GetAllPictures()
        {
            var pictures = await _context.Pictures.ToListAsync();
            return pictures;
        }

        public async Task<List<Picture>> GetPicturesByUserId(int userId)
        {
            var pictures = await _context.Pictures.Where(p => p.UserId == userId).OrderByDescending(p => p.DateUploaded).ToListAsync();
            return pictures;
        }

        public async Task<Picture> GetPictureById(int id)
        {
            var picture = await _context.Pictures.FirstOrDefaultAsync(p => p.PictureId == id);
            return picture;
        }

        // public async Task DownloadById(int id)
        // {

        // }

        public async Task Delete(int id)
        {
            var picture = _context.Set<Picture>().FirstOrDefault(p => p.PictureId == id);
            // guard against it coming back null
            var picturePath = Path.Combine(Directory.GetCurrentDirectory(), picture.Path);
            try
            {
                File.Delete(picturePath);
                _context.Pictures.Remove(picture);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            { 
            
            }
        }
        // I don't know if I need this
        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}
