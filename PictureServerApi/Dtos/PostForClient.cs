using System;
using System.ComponentModel.DataAnnotations;
using PictureServerApi.Models;

namespace PictureServerApi.Dtos
{
    public class PostForClient
    {
        public int PostId { get; set; }
        public string Story { get; set; }
        public int Likes { get; set; }
        public long PictureId { get; set; }
        public DateTime DateCreated { get; set; }
        public UserForClient User { get; set; }
    }
}