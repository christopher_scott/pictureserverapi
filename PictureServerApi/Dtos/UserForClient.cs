using System;
using PictureServerApi.Models;

namespace PictureServerApi.Dtos
{
    public class UserForClient
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int ProfilePictureId { get; set; }
        public DateTime DateJoined { get; set; }
        public string FullName { get; set; }
        public string Hometown { get; set; }
        public int Age { get; set; }
    }
}