using System;

namespace PictureServerApi.Dtos
{
    public class FriendForClientDto
    {
        public int UserId { get; set; }
        public int UserIdOfFriend { get; set; }
        public DateTime DateFriended { get; set; }
    }
}