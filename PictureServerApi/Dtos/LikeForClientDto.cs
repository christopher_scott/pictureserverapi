using PictureServerApi.Models;

namespace PictureServerApi.Dtos
{
    public class LikeForClientDto
    {
        public int LikeId { get; set; }
        public UserForClient User { get; set; }
        public int PostId { get; set; }
    }
}