using System;

namespace PictureServerApi.Dtos
{
    public class PictureForClientDto
    {
        public long PictureId { get; set; }
        public string Name { get; set; }
        public DateTime DateUploaded { get; set; }
        public long UserId { get; set; }
    }
}