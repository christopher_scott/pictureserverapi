namespace PictureServerApi.Dtos
{
    public class LikeForCreateDto
    {
        public int PostId { get; set; }
        public int UserId { get; set; }
    }
}