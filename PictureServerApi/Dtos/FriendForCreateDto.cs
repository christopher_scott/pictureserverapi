namespace PictureServerApi.Dtos
{
    public class FriendForCreateDto
    {
        public int UserId { get; set; }
        public int UserIdOfFriend { get; set; }
    }
}