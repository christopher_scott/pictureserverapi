namespace PictureServerApi.Dtos
{
    public class UserForPatchDto
    {
        public string FullName { get; set; }
        public string Hometown { get; set; }
        public int Age { get; set; }
        public int ProfilePictureId { get; set; }
    }
}