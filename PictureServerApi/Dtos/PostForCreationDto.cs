namespace PictureServerApi.Dtos
{
    public class PostForCreationDto
    {
        public string Story { get; set; }
        public int PictureId { get; set; } = 1;
        public int UserId { get; set; } = 1;
    }
}