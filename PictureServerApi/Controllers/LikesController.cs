using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using PictureServerApi.Data;
using PictureServerApi.Dtos;

namespace PictureServerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LikesController : ControllerBase
    {
        private readonly ILikesRepository _repo;

        public LikesController(ILikesRepository repo)
        {
            _repo = repo;
        }

        [HttpPost]
        public async Task<IActionResult> CreateLike(LikeForCreateDto like)
        {
            // if the user who is sending this create already has a like for this post, we should not create it.
            try
            {
                var likeToReturn = await _repo.Create(like);
                return Ok(likeToReturn);
            }
            catch(Exception ex)
            {
                return StatusCode(500);
            }
        }
        // delete like
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePostById(int id)
        {
            try
            {
                await _repo.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }

        // get likes
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPostsByUserId(int id)
        {
            try
            {
                var likes = await _repo.GetLikesByPostId(id);
                return Ok(likes);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }
    }
}