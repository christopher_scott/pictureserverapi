using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using PictureServerApi.Data;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FriendsController : ControllerBase
    {
         private readonly IFriendsRepository _repo;

        public FriendsController(IFriendsRepository repo)
        {
            _repo = repo;
        }

        [HttpPost]
        public async Task<IActionResult> CreateFriend(FriendForCreateDto friend)
        {
            try
            {
                if (await _repo.FriendRelationshipExists(friend.UserId, friend.UserIdOfFriend))
                {
                    return BadRequest("Friend relationship already exists");
                }
                    
                var friendToReturn = await _repo.Create(friend);
                return Ok(friendToReturn);
            }
            catch(Exception ex)
            {
                return StatusCode(500);
            }
        }
        
        [HttpDelete()]
        public async Task<IActionResult> RemoveFriend(FriendForCreateDto friend)
        {
            try
            {
                await _repo.Delete(friend);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }

        // get friends by userId
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFriendsByUserId(int id)
        {
            try
            {
                var friends = await _repo.GetFriendsByUserId(id);
                return Ok(friends);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }

        [HttpGet("relationship")]
        public async Task<IActionResult> GetFriendsRelationship(int userId, int userIdOfFriend)
        {
            try
            {
                var friends = await _repo.FriendRelationshipExists(userId, userIdOfFriend);
                return Ok(new { UsersAreFriends = friends });
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }
    }
}
