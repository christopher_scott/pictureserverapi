using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PictureServerApi.Data;
using PictureServerApi.Models;

namespace PictureServerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PicturesController : ControllerBase
    {
        public IPictureRepository _repository { get; set; }

        public PicturesController(IPictureRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Pictures
        [HttpGet]
        public async Task<IActionResult> GetAllPictures()
        {
            try
            {
                var pictures = await _repository.GetAllPictures();
                return Ok(pictures);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }            
        }

        // GET: api/Pictures/5
        [HttpGet("{id}", Name = "GetById")]
        public async Task<IActionResult> GetPictureById(int id)
        {
            try
            {
                var picture = await _repository.GetPictureById(id);

                // fix this
                if (picture == null)
                {
                    //_logger.LogError($"Owner with id: {id}, hasn't been found in the db.");
                    return NotFound();
                }
                else
                {
                    //_logger.LogInfo($"Returned owner with id: {id}");
                    return Ok(picture);
                }
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // do this for now, you will want to protect this route. Figure out how to do this with a token
        [AllowAnonymous]
        // GET: api/Pictures/download/5
        [HttpGet("download/{id}", Name = "DownloadById")]
        public async Task<IActionResult> DownloadPictureById(int id)
        {
            // do this all in the controller first. Once that works, try to move the appropriate logic to a service
            var picture = await _repository.GetPictureById(id);

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           picture.Path);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, getContentType(path), Path.GetFileName(path));
        }

        // GET: api/Pictures/user/5
        [HttpGet("user/{userId}", Name = "GetByUserId")]
        public async Task<IActionResult> GetPicturesByUserId(int userId)
        {
            try
            {
                var pictures = await _repository.GetPicturesByUserId(userId);
                return Ok(pictures);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // POST: api/Pictures
        [HttpPost, DisableRequestSizeLimit] // revisit this, may want to do a chunked upload for larger pics
        public async Task<IActionResult> Upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                // you will want to change this eventually to make it configurable
                // you're also going to want to figure out how to have the code make the folder
                var folderName = Path.Combine("Resources", "Images");
                var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                var name = dict["Name"];
                // make this more robust -- this could throw an exception
                var userId = Int32.Parse(dict["UserId"]);
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0)
                {
                    // we should probably move this logic out into some service layer
                    // check to see if the filename exists before you do this
                    // another idea would be to handle making file names differently, like incrementally or something
                    // or maybe a guid
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fileExtension = Path.GetExtension(fileName);
                    var fileGuid = Guid.NewGuid();
                    fileName = $"{fileGuid}{fileExtension}";
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);

                    var picture = new Picture() { FileName = file.FileName, Path = dbPath , Name = name, UserId = userId };

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    var pictureResponse = await _repository.Create(picture);
                    //await _repository.Save();

                    return Ok(pictureResponse);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT: api/Pictures/5
        // This should be to update information? How would I do that?
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            // I think you should consider doing this more like what we do in ShareBase, have a flag
            // that the file is deleted, but not purged. Then just move it in the UI
            try
            {
                // should this stuff live here? Figure out what the correct pattern is
                var picture = await _repository.GetPictureById(id);
                var pathToDelete = Path.Combine(Directory.GetCurrentDirectory(), picture.Path);
                System.IO.File.Delete(pathToDelete);

                await _repository.Delete(id);
                //await _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
            // try to delete the file from the filesystem
            // get the file path from the db


            // if this deletion succeeds, we can then delete the entry from the database

            // ultimately, we want to only be able to delete the file if the file creator is trying to delete it

            // also, there should be some concept of a god-mode user who can delete anything (sys admin)
        }

        // take a look at this, I copied it wholesale from this:
        // https://www.codeproject.com/articles/1203408/upload-download-files-in-asp-net-core
        // There's probably some work to be done to improve this
        private string getContentType(string path)
        {
            var types = getMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> getMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}
