using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PictureServerApi.Data;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        public IPostsRepository _repository { get; set; }
        public PostsController(IPostsRepository repository)
        {
            _repository = repository;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAllPosts()
        {
            try
            {
                var posts = await _repository.GetAllPosts();
                return Ok(posts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }

        //[HttpGet("{id}", Name = "GetById")]
        // change this to be consistent -- post/{id}
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPostById(int id)
        {
            try
            {
                var post = await _repository.GetPostById(id);
                return Ok(post);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }

        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetPostsByUserId(int id)
        {
            try
            {
                var posts = await _repository.GetPostsByUserId(id);
                return Ok(posts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }

        [HttpGet("user/timeline/{userId}")]
        public async Task<IActionResult> GetPostsForTimeline(int userId)
        {
            try
            {
                var posts = await _repository.GetPostsForTimeline(userId);
                return Ok(posts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 

        }

        [HttpPost]
        public async Task<IActionResult> CreatePost(PostForCreationDto postToCreate)
        {
            try
            {
                // you can probably move this into the repo
                var post = new Post
                {
                    Story = postToCreate.Story,
                    PictureId = postToCreate.PictureId,
                    UserId = postToCreate.UserId,
                    DateCreated = DateTime.UtcNow
                };

                var postToReturn = await _repository.Create(post);
                return Ok(postToReturn);
            }
            catch(Exception ex)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePostById(int id)
        {
            try
            {
                await _repository.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            } 
        }

        // you will eventually want to be able to update a post, to change the story
        // likes -- you may want a separate table for the likes or something, 
        // rather than them being part of the post table
    }
}