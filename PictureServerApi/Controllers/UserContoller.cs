using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PictureServerApi.Data;
using PictureServerApi.Dtos;

namespace PictureServerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]

    public class UserController : ControllerBase
    {
        private readonly IUserRepository _repo;

        public UserController(IUserRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var users = await _repo.GetAllUsers();
                return Ok(users);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        // get user by id
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUsersById(int id)
        {
            try
            {
                var user = await _repo.GetUserById(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [Route("patch/{id}")]
        [HttpPatch]
        public async Task<IActionResult> Patch(int id, [FromBody] JsonPatchDocument<UserForPatchDto> patchModel)
        {
            try
            {
                var userFromDb = await _repo.GetUserById(id);
                if (userFromDb == null) return NotFound();
                var userForClient = await _repo.UpdateUser(id, patchModel);

                return Ok(userForClient);
            }
            catch (Exception e)
            {
            }

            return BadRequest("Error updating user");
        }
    }
}