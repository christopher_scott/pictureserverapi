using AutoMapper;
using PictureServerApi.Dtos;
using PictureServerApi.Models;

namespace PictureServerApi.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Post, PostForClient>();
            CreateMap<User, UserForClient>();
            CreateMap<User, UserForPatchDto>();
            CreateMap<UserForPatchDto, User>();
            CreateMap<Picture, PictureForClientDto>();
            CreateMap<Like, LikeForClientDto>();
            CreateMap<Friend, FriendForClientDto>();
        }
    }
}