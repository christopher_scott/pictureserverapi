using System;
using System.Collections.Generic;

namespace PictureServerApi.Models
{
    public class User
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime DateJoined { get; set; }
        public bool Active { get; set; }
        public int ProfilePictureId { get; set; }
        public string FullName { get; set; }
        public string Hometown { get; set; }
        public int Age { get; set; }
    }
}