using System;
using System.Collections.Generic;

namespace PictureServerApi.Models
{
    public class Post
    {
        public int PostId { get; set; }
        public string Story { get; set; }
        // public int Likes { get; set; }
        public long PictureId { get; set; }
        public Picture Picture { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public DateTime DateCreated { get; set; }
        public List<Like> Likes { get; set; }
    }
}