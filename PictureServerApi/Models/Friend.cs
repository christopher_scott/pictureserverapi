using System;

namespace PictureServerApi.Models
{
    public class Friend
    {
        public int FriendId { get; set; }
        public int UserId { get; set; }
        public int UserIdOfFriend { get; set; }
        public DateTime DateFriended { get; set; }
    }
}