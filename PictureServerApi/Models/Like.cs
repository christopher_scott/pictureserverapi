namespace PictureServerApi.Models
{
    public class Like
    {
        public int LikeId { get; set; }
        public User User { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
    }
}