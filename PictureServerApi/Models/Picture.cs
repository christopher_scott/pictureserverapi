using System;
using System.Linq;
using System.Threading.Tasks;

namespace PictureServerApi.Models
{

    public class Picture
    {
        public long PictureId { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public DateTime DateUploaded { get; set; }

        // I'm not sure this is right. Why do I need user and userId?
        public long UserId { get; set; }
        public User User { get; set; }
    }
}
