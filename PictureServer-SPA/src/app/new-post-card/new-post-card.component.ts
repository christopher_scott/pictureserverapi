import { Component, OnInit, Input } from '@angular/core';
import { Picture } from 'src/_models/picture';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-new-post-card',
  templateUrl: './new-post-card.component.html',
  styleUrls: ['./new-post-card.component.css']
})
export class NewPostCardComponent implements OnInit {
  @Input() picture: Picture;
  constructor(private http: HttpClient, private router: Router, public authService: AuthService) { }

  ngOnInit() {
  }

  createNewPost(postStory: string) {
    // this parseInt might be crap. See if there's a better way to do this
    const post = { story: postStory, userId: parseInt(this.authService.decodedToken.nameid, 10), pictureId: this.picture.pictureId };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    this.http.post('http://localhost:5000/api/posts/', post, httpOptions )
      .subscribe(response => {
        this.router.navigate(['timeline']);
      });
  }
}
