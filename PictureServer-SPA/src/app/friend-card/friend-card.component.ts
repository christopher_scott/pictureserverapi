import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/_models/user';
import { FriendsService } from '../_services/friends.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-friend-card',
  templateUrl: './friend-card.component.html',
  styleUrls: ['./friend-card.component.css']
})
export class FriendCardComponent implements OnInit {
  @Input() friend: User;
  @Input() loggedInUser: User;
  usersAreFriends = false;
  friendResponse: any;

  constructor(private friendsService: FriendsService, private http: HttpClient) {

  }

  ngOnInit() {
    // this.friendsService.areUsersFriends(this.loggedInUser.userId, this.friend.userId).subscribe(response => {
    //   this.usersAreFriends = response.usersAreFriends;
    // });
    const options = {
      // headers: new HttpHeaders({
      //   'Content-Type': 'application/json',
      // }),
      params: {
        userId: `${this.loggedInUser.userId}`,
        userIdOfFriend: `${this.friend.userId}`
      }
    };
    this.http.get('http://localhost:5000/api/friends/relationship', options).subscribe(response => {
      this.friendResponse = response;
      this.usersAreFriends = this.friendResponse.usersAreFriends;
    });


    // this.httpClient.get('/url', {
    //   params: {
    //     appid: 'id1234',
    //     cnt: '5'
    //   },
    //   observe: 'response'
    // })
  }

}
