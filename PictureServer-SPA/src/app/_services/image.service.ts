import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../_services/auth.service';

@Injectable({
  providedIn: 'root'
})

export class ImageService {

  constructor(private http: HttpClient, public authService: AuthService) { }

  // this is all named horribly, fix it
  // is the <any> okay, or do I need to be more specific?
  getImagesInfo(imageInfoUrl: string): Observable<any> {
    return this.http.get(imageInfoUrl);
  }

  getImageInfoByUserId(id: number): Observable<any> {
    return this.http.get(`http://localhost:5000/api/pictures/user/${id}`);
  }

  getImage(imageUrl: string): Observable<Blob> {
    return this.http.get(imageUrl, { responseType: 'blob' });
  }

  uploadPicture(files: any): Observable<any> {
    const fileToUpload = files[0] as File;
    const formData = new FormData();
    formData.append(fileToUpload.name, fileToUpload);
    formData.append('Name', 'this_is_a_filename');
    formData.append('UserId', this.authService.decodedToken.nameid);

    return this.http.post('http://localhost:5000/api/pictures/', formData, {reportProgress: true, observe: 'events'});
  }
}
