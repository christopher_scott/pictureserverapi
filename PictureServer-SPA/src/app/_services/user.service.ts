import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { User } from 'src/_models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  loggedInUser: User;

  constructor(private authService: AuthService, private http: HttpClient) { }

  getLoggedInUser(): Observable<any> {
     return this.http.get(`http://localhost:5000/api/user/${parseInt(this.authService.decodedToken.nameid, 10)}`);
  }

  updateUser(model: any): Observable<any> {
    return this.http.patch(`http://localhost:5000/api/user/patch/${parseInt(this.authService.decodedToken.nameid, 10)}`, model);
  }

  // shouldn't this return an observable? Fix it.
  getUserById(id: number) {
    return this.http.get(`http://localhost:5000/api/user/${id}`);
  }

  getAllUsers(): Observable<any> {
    return this.http.get('http://localhost:5000/api/user/');
  }
}
