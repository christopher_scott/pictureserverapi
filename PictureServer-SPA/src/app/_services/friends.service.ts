import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FriendsService {
  constructor(private http: HttpClient) {}

  getFriendsForUser(userId: number): Observable<any> {
    return this.http.get(`http://localhost:5000/api/friends/${userId}`);
  }

  addFriend(userId: number, userIdOfFriend: number): Observable<any> {
    const body = { UserId: userId, UserIdOfFriend: userIdOfFriend };
    return this.http.post(`http://localhost:5000/api/friends`, body);
  }

  removeFriend(userId: number, userIdOfFriend: number): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        UserId: userId,
        UserIdOfFriend: userIdOfFriend,
      },
    };
    return this.http.delete(`http://localhost:5000/api/friends`, options);
  }

  areUsersFriends(userid: number, userIdOfFriend: number): Observable<any> {
    // const options = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json',
    //   })
    // };
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      params: {
        userId: `${userid}`,
        userIdOfFriend: `${userIdOfFriend}`
      }
    };
    return this.http.get('http://localhost:5000/api/friends/relationship', options);
  }
}
