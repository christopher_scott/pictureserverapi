import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewPostComponent } from './new-post/new-post.component';
import { PostComponent } from './post/post.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ChangeProfilePicComponent } from './change-profile-pic/change-profile-pic.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { DiscoverFriendsComponent } from './discover-friends/discover-friends.component';

const routes: Routes = [
  { path: '', redirectTo: '/timeline', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'timeline', component: PostComponent },
  { path: 'new-post', component: NewPostComponent },
  { path: 'my-posts', component: MyPostsComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'profile-edit', component: ProfileEditComponent },
  { path: 'change-profile-pic', component: ChangeProfilePicComponent },
  { path: 'view-profile/:id', component: ViewProfileComponent },
  { path: 'discover-friends', component: DiscoverFriendsComponent },
  { path: '**', component: PostComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
