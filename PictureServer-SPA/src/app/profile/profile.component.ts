import { Component, OnInit } from '@angular/core';
import { User } from 'src/_models/user';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loggedInUser: User;

  constructor(public userService: UserService, private router: Router) { }
  patchModel = [];

  ngOnInit() {
    this.getLoggedInUser();
  }

  getLoggedInUser() {
    this.userService.getLoggedInUser().subscribe(response => {
      this.loggedInUser = response;
    });
  }

  deleteProfilePic() {
    this.patchModel.push({ op: 'replace', path: 'profilePictureId', value: 0 });
    this.userService.updateUser(this.patchModel).subscribe(response => {
      this.loggedInUser.profilePictureId = 0;
      this.router.navigate(['profile']);
    }); // do something with any errors eventually
  }
}
