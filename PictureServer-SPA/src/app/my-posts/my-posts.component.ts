import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ImageService } from '../_services/image.service';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.css']
})
export class MyPostsComponent implements OnInit {
  postsToShow;
  toggle = true;

  constructor(private http: HttpClient, private imageService: ImageService, public authService: AuthService) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    this.http.get(`http://localhost:5000/api/posts/user/${parseInt(this.authService.decodedToken.nameid, 10)}`).subscribe(response => {
      this.postsToShow = response;
    }, error => {
      console.log(error);
    });
  }
}
