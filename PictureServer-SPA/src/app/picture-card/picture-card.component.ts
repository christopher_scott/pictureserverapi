import { Component, OnInit, Input } from '@angular/core';
import { Picture } from 'src/_models/picture';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';
import { User } from 'src/_models/user';

@Component({
  selector: 'app-picture-card',
  templateUrl: './picture-card.component.html',
  styleUrls: ['./picture-card.component.css']
})
export class PictureCardComponent implements OnInit {
  @Input() picture: Picture;
  patchModel = [];
  loggedInUser: User;

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.getLoggedInUser();
  }

  updateProfilePic() {
    this.patchModel.push({ op: 'replace', path: 'profilePictureId', value: this.picture.pictureId });
    this.userService.updateUser(this.patchModel).subscribe(response => {
      this.router.navigate(['/view-profile/' + this.loggedInUser.userId]);
    }); // do something with any errors eventually
  }

  getLoggedInUser() {
    this.userService.getLoggedInUser().subscribe(response => {
      this.loggedInUser = response;
    });
  }
}
