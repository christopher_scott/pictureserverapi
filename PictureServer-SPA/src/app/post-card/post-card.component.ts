import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/_models/user';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Post } from 'src/_models/post';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {
  @Input() post: Post;
  clicked = false;
  likes;
  currentUserLike;
  currentUserLikedPost = false;

  constructor(private http: HttpClient, public authService: AuthService, public router: Router) { }

  ngOnInit() {
    this.getLikes();
  }

  getLikes() {
    this.http.get(`http://localhost:5000/api/likes/${this.post.postId}`).subscribe(response => {
      this.likes = response;
      // do something here to set currentUserLikedPost
      // if likes contains a like with userId that matches current logged in user userId, set to true
      this.currentUserLikedPost = this.likes.some(l => l.user.userName === this.authService.decodedToken.unique_name);
    });
  }

  getLikesUserNames() {
    let content = '';
    this.likes.forEach(like => {
      content += like.user.userName;
      content += '<br>';
    });

    return content;
  }

  addLikeForCurrentUser() {
    const like = { userId: parseInt(this.authService.decodedToken.nameid, 10), postId: this.post.postId };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    this.http.post('http://localhost:5000/api/likes/', like, httpOptions )
      .subscribe(() => {
        this.getLikes();
        // maybe not necessary? Try it.
        this.currentUserLikedPost = true;
      });
  }

  removeLikeForCurrentUser() {
    console.log(this.likes);
    const likeForLoggedInUser = this.likes.find(l => l.user.userId === parseInt(this.authService.decodedToken.nameid, 10));
    console.log(likeForLoggedInUser);
    this.http.delete(`http://localhost:5000/api/likes/${likeForLoggedInUser.likeId}`)
      .subscribe(() => {
        this.getLikes();
        // maybe not necessary? Try it.
        this.currentUserLikedPost = false;
      });
  }
}
