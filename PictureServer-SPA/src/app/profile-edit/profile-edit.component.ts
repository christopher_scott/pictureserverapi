import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';
import { User } from 'src/_models/user';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {
  model: any = {};
  loggedInUser: User;
  patchModel = [];

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.getLoggedInUser();
  }

  update() {
    if (this.model.age) {
      this.patchModel.push({ op: 'replace', path: 'age', value: this.model.age });
    }
    if (this.model.hometown) {
      this.patchModel.push({ op: 'replace', path: 'hometown', value: this.model.hometown });
    }
    if (this.model.fullName) {
      this.patchModel.push({ op: 'replace', path: 'fullName', value: this.model.fullName });
    }

    this.userService.updateUser(this.patchModel).subscribe(response => {
      this.router.navigate(['/view-profile/' + this.loggedInUser.userId]);
    }); // do something with any errors eventually
  }

  cancel() {
    this.router.navigate(['/view-profile/' + this.loggedInUser.userId]);
  }

  getLoggedInUser() {
    this.userService.getLoggedInUser().subscribe(response => {
      this.loggedInUser = response;
    });
  }
}
