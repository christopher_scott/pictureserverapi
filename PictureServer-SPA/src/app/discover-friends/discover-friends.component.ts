import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { User } from 'src/_models/user';

@Component({
  selector: 'app-discover-friends',
  templateUrl: './discover-friends.component.html',
  styleUrls: ['./discover-friends.component.css']
})
export class DiscoverFriendsComponent implements OnInit {

  constructor(private userService: UserService) { }
  users: User[];
  loggedInUser: User;

  ngOnInit() {
    this.getAllUsers();
    this.userService.getLoggedInUser().subscribe(user => {
      this.loggedInUser = user as User;
    });
  }

  // make this, like, get all users
  getAllUsers() {
    this.userService.getAllUsers().subscribe((response) => {
      this.users = response;
    });
  }
}
