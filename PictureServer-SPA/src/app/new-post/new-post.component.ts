import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { Picture } from 'src/_models/picture';
import { Post } from 'src/_models/post';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { ImageService } from '../_services/image.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  public progress: number;
  public message: string;
  public pictureUrl: string;
  public pictureUploaded = false;
  public picture: Picture;

  @Output() public onUploadFinished = new EventEmitter();

  constructor(private http: HttpClient, private router: Router, public authService: AuthService, public imageService: ImageService) { }

  ngOnInit() {
  }

  // This could use some cleaning up, and the service, too -- check out the event stuff, figure out what's needed
  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }

    this.imageService.uploadPicture(files)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';
          // I'm not entirely sure what any of this event stuff does or why I need it
          // this.onUploadFinished.emit(event.body);
          this.picture = event.body as Picture;
          this.pictureUrl = `http://localhost:5000/api/pictures/download/${this.picture.pictureId}`;
          this.pictureUploaded = true;
        }
      });
  }
}
