import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AuthService } from './_services/auth.service';
import { PostComponent } from './post/post.component';
import { PostCardComponent } from './post-card/post-card.component';
import { AppRoutingModule } from './app-routing.module';
import { NewPostComponent } from './new-post/new-post.component';
import { NewPostCardComponent } from './new-post-card/new-post-card.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ChangeProfilePicComponent } from './change-profile-pic/change-profile-pic.component';
import { PictureCardComponent } from './picture-card/picture-card.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { FriendCardComponent } from './friend-card/friend-card.component';
import { DiscoverFriendsComponent } from './discover-friends/discover-friends.component';
import { LoadingComponent } from './loading/loading.component';

export function tokenGetter() {
   return localStorage.getItem('token');
}

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      PostComponent,
      PostCardComponent,
      NewPostComponent,
      NewPostCardComponent,
      MyPostsComponent,
      HomeComponent,
      RegisterComponent,
      ProfileComponent,
      ProfileEditComponent,
      ChangeProfilePicComponent,
      PictureCardComponent,
      ViewProfileComponent,
      FriendCardComponent,
      DiscoverFriendsComponent,
      LoadingComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      PopoverModule.forRoot(),
      BsDropdownModule.forRoot(),
      JwtModule.forRoot({
         config: {
            tokenGetter: tokenGetter,
            allowedDomains: ['localhost:5000'],
            disallowedRoutes: ['localhost:5000/api/auth']
         }
      })
   ],
   providers: [
      AuthService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
