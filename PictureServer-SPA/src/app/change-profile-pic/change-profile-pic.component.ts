import { Component, OnInit } from '@angular/core';
import { ImageService } from '../_services/image.service';
import { UserService } from '../_services/user.service';
import { User } from 'src/_models/user';
import { Picture } from 'src/_models/picture';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-change-profile-pic',
  templateUrl: './change-profile-pic.component.html',
  styleUrls: ['./change-profile-pic.component.css']
})
export class ChangeProfilePicComponent implements OnInit {
  pictures: any;
  public progress: number;
  public message: string;
  public pictureUrl: string;
  public pictureUploaded = false;
  public picture: Picture;

  constructor(private imageService: ImageService, private userService: UserService) { }

  ngOnInit() {
    this.getImageInfo();
  }

  getImageInfo() {
    this.userService.getLoggedInUser().subscribe(response => {
      this.imageService.getImageInfoByUserId(response.userId).subscribe(result => {
        this.pictures = result;
      });
    });
  }

    // This could use some cleaning up, and the service, too -- check out the event stuff, figure out what's needed
    public uploadFile = (files) => {
      if (files.length === 0) {
        return;
      }

      this.imageService.uploadPicture(files)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          } else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.';
            // I'm not entirely sure what any of this event stuff does or why I need it
            // this.onUploadFinished.emit(event.body);
            this.picture = event.body as Picture;
            this.pictureUrl = `http://localhost:5000/api/pictures/download/${this.picture.pictureId}`;
            this.pictureUploaded = true;
            this.getImageInfo();
          }
        });
    }
}
