import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { User } from "src/_models/user";
import { UserService } from "../_services/user.service";
import { FriendsService } from "../_services/friends.service";
import { Router } from "@angular/router";
import { switchMap } from "rxjs/operators";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-view-profile",
  templateUrl: "./view-profile.component.html",
  styleUrls: ["./view-profile.component.css"],
})
export class ViewProfileComponent implements OnInit {
  userId: number;
  user: User;
  friends: User[];
  loggedInUser: User;
  userIsFriend = false;
  userIsLoggedInUser = false;
  patchModel = [];
  loading = false;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private friendsService: FriendsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loading = true;

    this.route.params
      .pipe(
        switchMap((params) => {
          this.userId = params.id;

          return this.userService.getUserById(this.userId);
        })
      )
      .pipe(
        switchMap((user) => {
          this.user = user as User;

          return this.friendsService.getFriendsForUser(this.user.userId);
        })
      )
      .pipe(
        switchMap((friends) => {
          this.friends = friends as User[];

          return this.userService.getLoggedInUser();
        })
      )
      .subscribe((user) => {
        this.loggedInUser = user as User;
        this.isUserLoggedInUser();
        this.isLoggedInUserFriendsWithUser(
          this.friends,
          this.loggedInUser.userId
        );
        this.loading = false;
      });
  }

  isUserLoggedInUser() {
    if (this.user.userId === this.loggedInUser.userId) {
      this.userIsLoggedInUser = true;
    }
  }

  getFriendsForUser(userId: number) {
    this.friendsService.getFriendsForUser(userId).subscribe((response) => {
      this.friends = response;
      this.isLoggedInUserFriendsWithUser(
        this.friends,
        this.loggedInUser.userId
      );
    });
  }

  isLoggedInUserFriendsWithUser(friends: any, userId: number) {
    const friend = friends.find((f) => f.userId === userId);
    if (friend) {
      this.userIsFriend = true;
    }
  }

  addFriend() {
    this.friendsService
      .addFriend(this.loggedInUser.userId, this.user.userId)
      .subscribe((response) => {
        this.userIsFriend = true;
      });
  }

  removeFriend() {
    this.friendsService
      .removeFriend(this.loggedInUser.userId, this.user.userId)
      .subscribe((response) => {
        this.userIsFriend = false;
      });
  }

  deleteProfilePic() {
    this.patchModel.push({ op: "replace", path: "profilePictureId", value: 0 });
    this.userService.updateUser(this.patchModel).subscribe((response) => {
      this.loggedInUser.profilePictureId = 0;
      this.router.navigate([`/view-profile/${this.loggedInUser.userId}`]);
    }); // do something with any errors eventually
  }
}
