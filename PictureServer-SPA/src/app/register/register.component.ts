import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter();
  model: any = {};
  registerSuccess = false;
  registerError = false;
  error: string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  cancel() {
    this.cancelRegister.emit(false);
  }

  register() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    this.http.post('http://localhost:5000/api/auth/register', this.model, httpOptions).subscribe(response => {
      this.registerSuccess = true;
      this.registerError = false;
    }, error => {
      console.log(error);
      this.error = error.error;
      this.registerSuccess = false;
      this.registerError = true;
    });
  }
}
