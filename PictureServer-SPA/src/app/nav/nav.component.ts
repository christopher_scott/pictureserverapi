import { Component, OnInit, Injectable } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';
import { User } from 'src/_models/user';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})

export class NavComponent implements OnInit {
  model: any = {};
  loggedInUser: User;

  constructor(public authService: AuthService, private router: Router, private userService: UserService) { }

  ngOnInit() {
    // this.login();
    this.getLoggedInUser();
  }

  login() {
    console.log(this.model);
    return this.authService.login(this.model).subscribe(next => {
      console.log('Logged in successfully');
      this.router.navigate(['timeline']);
    }, error => {
      console.log('Failed to login');
    });
  }

  loggedIn() {
    return this.authService.loggedIn();
  }

  logout() {
    localStorage.removeItem('token');
    console.log('logged out');
    this.router.navigate(['home']);
  }

  getLoggedInUser() {
    this.userService.getLoggedInUser().subscribe(response => {
      this.loggedInUser = response;
    });
  }
}
