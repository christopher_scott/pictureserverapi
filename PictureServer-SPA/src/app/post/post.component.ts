import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ImageService } from '../_services/image.service';
import { UserService } from '../_services/user.service';
import { User } from 'src/_models/user';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent implements OnInit {
  postsToShow;
  toggle = true;
  loggedInUser: User;

  constructor(private http: HttpClient, private imageService: ImageService, private userService: UserService) {
  }

  ngOnInit() {
    this.getPosts();
  }

  // this can live here for now, move things to a service eventually?

  getPosts() {
    this.userService.getLoggedInUser()
      .subscribe(response => {
        this.loggedInUser = response as User;
        this.http.get(`http://localhost:5000/api/posts/user/timeline/${this.loggedInUser.userId}`).subscribe(result => {
          this.postsToShow = result;
        }, error => {
          console.log(error);
        });
      });
  }
}
