export interface Picture {
    pictureId: number;
    name: string;
    dateUploaded: Date;
    userId: number;
}
