export interface User {
    userId: number;
    userName: string;
    profilePictureId: number;
    fullName: string;
    age: number;
    hometown: string;
    dateJoined: Date;
}
