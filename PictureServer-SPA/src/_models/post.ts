import { User } from './user';

export interface Post {
    postId: number;
    story: string;
    likes: number;
    pictureId: number;
    dateCreated: Date;
    user: User;
}
